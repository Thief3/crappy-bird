﻿using UnityEngine;
using System.Collections;

public class Tap : MonoBehaviour {
    public float upwards;
    private Rigidbody2D rb;
	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButton("Jump")) {
            rb.velocity = new Vector2(rb.velocity.x,  upwards);
        }
	
	}
}
