﻿using UnityEngine;
using System.Collections;

public class pipeRelocate : MonoBehaviour {
    public float min;
    public float max;

    void Start() {
        transform.position = new Vector3(transform.position.x, 
            Random.Range(min, max),
            0);
    }

	void OnBecameInvisible() {
        transform.position = new Vector3 (9, 
            Random.Range(min, max), 
            0);

    }
}
