﻿using UnityEngine;
using System.Collections;

public class GameOver : MonoBehaviour {
    public GameObject score;

	// Use this for initialization
	void Start () {
        Invoke("Load", 2);
    }
	
    void Load() {
        Application.LoadLevel(Application.loadedLevel);
    }
}
