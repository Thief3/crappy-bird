﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoringCollision : MonoBehaviour {
    public GameObject scoreDisplay;
    private float score;
    private Text text;

    void Start() {
        score = 0;
        text = scoreDisplay.GetComponent<Text>();
    }

    void OnTriggerExit2D() {
       Debug.Log("h");
       score += 0.5f;
       text.text = score.ToString();
    }
}
