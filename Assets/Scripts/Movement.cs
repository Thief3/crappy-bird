﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour {
    public float speed;
    private Rigidbody2D rb;
	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = new Vector2(speed, 0);
	}
	
}
