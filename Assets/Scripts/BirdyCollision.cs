﻿using UnityEngine;
using System.Collections;

public class BirdyCollision : MonoBehaviour {

    void Start() {

    }

	void OnCollisionEnter2D(Collision2D coll) {
        GetComponent<GameOver>().enabled = true;
        GetComponent<CircleCollider2D>().enabled = false;
    }
}
